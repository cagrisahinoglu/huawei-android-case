package com.cagrisahinoglu.huawei.data.response

import com.google.gson.annotations.SerializedName

data class TeamResponse(
    @SerializedName("name")
    val name: String
)
