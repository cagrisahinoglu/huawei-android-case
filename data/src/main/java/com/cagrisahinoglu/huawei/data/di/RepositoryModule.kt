package com.cagrisahinoglu.huawei.data.di

import com.cagrisahinoglu.huawei.data.repository.TeamRepositoryImp
import com.cagrisahinoglu.huawei.domain.repository.TeamRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
interface RepositoryModule {

    @Singleton
    @Binds
    fun provideTeamRepository(teamRepositoryImp: TeamRepositoryImp): TeamRepository
}
