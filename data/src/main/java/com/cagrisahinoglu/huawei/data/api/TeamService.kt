package com.cagrisahinoglu.huawei.data.api

import com.cagrisahinoglu.huawei.data.response.TeamResponse
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface TeamService {

    @FormUrlEncoded
    @POST("teams.php")
    suspend fun getTeams(@Field("team_size") teamSize: Int): List<TeamResponse>
}
