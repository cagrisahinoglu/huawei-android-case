package com.cagrisahinoglu.huawei.data.mapper

import com.cagrisahinoglu.huawei.data.response.TeamResponse
import com.cagrisahinoglu.huawei.domain.model.Team

class TeamMapper : BaseMapper<TeamResponse, Team> {

    override fun mapToItem(data: TeamResponse): Team {
        return Team(
            name = data.name
        )
    }
}
