package com.cagrisahinoglu.huawei.data.util

sealed class ApiResult<out T> {

    data class Success<out T>(val data: T) : ApiResult<T>()

    data class Error(val error: Exception? = null) : ApiResult<Nothing>()

    object Loading : ApiResult<Nothing>()
}
