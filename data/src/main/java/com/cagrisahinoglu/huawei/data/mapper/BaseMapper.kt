package com.cagrisahinoglu.huawei.data.mapper

interface BaseMapper<Data, Domain> {

    fun mapToItem(data: Data): Domain

    fun mapToItem(data: Collection<Data>?): Collection<Domain> {
        return data?.map {
            mapToItem(it)
        } ?: emptyList()
    }

    fun mapToData(domain: Domain): Data? = null

    fun mapToData(domain: Collection<Domain>?): Collection<Data> {
        return domain?.map {
            mapToData(it)!!
        } ?: emptyList()
    }
}
