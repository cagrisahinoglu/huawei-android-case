package com.cagrisahinoglu.huawei.data.repository

import com.cagrisahinoglu.huawei.data.api.TeamService
import com.cagrisahinoglu.huawei.data.mapper.TeamMapper
import com.cagrisahinoglu.huawei.domain.model.Team
import com.cagrisahinoglu.huawei.domain.repository.TeamRepository
import javax.inject.Inject

class TeamRepositoryImp @Inject constructor(
    private val teamService: TeamService,
    private val teamMapper: TeamMapper
) : TeamRepository {

    override suspend fun getTeams(teamSize: Int): List<Team> {
        return teamService.getTeams(teamSize).map {
            teamMapper.mapToItem(it)
        }
    }
}
