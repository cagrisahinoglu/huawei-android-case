package com.cagrisahinoglu.huawei.domain.usecase

import com.cagrisahinoglu.huawei.domain.repository.TeamRepository
import javax.inject.Inject

class GetTeamUseCase @Inject constructor(
    private val teamRepository: TeamRepository
) {

    suspend fun getTeams(teamSize: Int) = teamRepository.getTeams(teamSize)
}
