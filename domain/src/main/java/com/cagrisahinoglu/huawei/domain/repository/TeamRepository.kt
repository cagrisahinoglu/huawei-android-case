package com.cagrisahinoglu.huawei.domain.repository

import com.cagrisahinoglu.huawei.domain.model.Team

interface TeamRepository {

    suspend fun getTeams(teamSize: Int): List<Team>
}
