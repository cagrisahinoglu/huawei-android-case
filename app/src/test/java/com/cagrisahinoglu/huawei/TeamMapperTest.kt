package com.cagrisahinoglu.huawei

import com.cagrisahinoglu.huawei.data.mapper.TeamMapper
import com.cagrisahinoglu.huawei.data.response.TeamResponse
import com.google.common.truth.Truth.assertThat
import org.junit.Test

class TeamMapperTest {

    @Test
    fun `Given BannerMapper and BannerResponse, when mapToItem called,Banner is mapped`() {
        val teamMapper = TeamMapper()
        val teamResponse = TeamResponse(
            name = "Team 1"
        )

        val team = teamMapper.mapToItem(teamResponse)

        assertThat(team.name).isEqualTo(teamResponse.name)
    }
}
