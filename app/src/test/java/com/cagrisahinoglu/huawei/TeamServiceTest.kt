package com.cagrisahinoglu.huawei

import com.cagrisahinoglu.huawei.data.api.TeamService
import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.runBlocking
import org.junit.Test

class TeamServiceTest : BaseServiceTest<TeamService>() {
    override fun setServiceClass(): Class<TeamService> = TeamService::class.java

    @Test
    fun `When getTeams is called, Then returns TeamResponse with passed values`() {
        enqueueResponse("mock-responses/team.json")

        runBlocking {
            val response = retrofitService.getTeams(10)[0]

            response.let {
                assertThat(it.name).isEqualTo("Team 1")
            }
        }
    }
}
