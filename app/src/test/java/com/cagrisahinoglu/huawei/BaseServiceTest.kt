package com.cagrisahinoglu.huawei

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okio.buffer
import okio.source
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@RunWith(JUnit4::class)
abstract class BaseServiceTest<S : Any> {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    protected lateinit var retrofitService: S
    protected val mockWebServer = MockWebServer()

    abstract fun setServiceClass(): Class<S>

    @Before
    fun setup() {
        retrofitService = getService()
    }

    @After
    fun teardown() {
        mockWebServer.shutdown()
    }

    private fun getService(): S {
        return Retrofit.Builder()
            .baseUrl(mockWebServer.url(""))
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(setServiceClass())
    }

    protected fun enqueueResponse(filePath: String, responseCode: Int? = null) {
        val inputStream = javaClass.classLoader?.getResourceAsStream(filePath)
        val bufferSource = inputStream?.source()?.buffer()
        val mockResponse = MockResponse().apply {
            setBody(bufferSource!!.readString(Charsets.UTF_8))
        }
        mockWebServer.enqueue(mockResponse)
    }

    protected fun enqueueErrorResponse(filePath: String, errorCode: Int) {
        enqueueResponse(filePath, errorCode)
    }

}
