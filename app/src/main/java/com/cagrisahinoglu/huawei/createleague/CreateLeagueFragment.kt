package com.cagrisahinoglu.huawei.createleague

import androidx.navigation.fragment.findNavController
import com.cagrisahinoglu.huawei.R
import com.cagrisahinoglu.huawei.base.BaseFragment
import com.cagrisahinoglu.huawei.databinding.FragmentCreateLeagueBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CreateLeagueFragment : BaseFragment<FragmentCreateLeagueBinding>() {
    override fun getLayoutId(): Int = R.layout.fragment_create_league

    override fun initViews() {
        binding.createLeagueButton.setOnClickListener {
            val teamSize = binding.createLeagueEditText.text.toString()
            if (teamSize.isNotEmpty() && teamSize.toInt() >= 2) {
                val action =
                    CreateLeagueFragmentDirections.actionCreateLeagueFragmentToTeamsFragment(
                        binding.createLeagueEditText.text.toString().toInt()
                    )
                findNavController().navigate(action)
            } else {
                showSnackBar(getString(R.string.create_league_warning))
            }
        }
    }
}
