package com.cagrisahinoglu.huawei.util

import com.cagrisahinoglu.huawei.domain.model.Match
import com.cagrisahinoglu.huawei.domain.model.Team

fun createLeagueFixture(teamList: List<Team>): List<List<Match>> {
    val teams = teamList.toMutableList()
    teams.shuffle()
    val numberOfRound = teams.size * (teams.size - 1) / (teams.size / 2)
    val isEvenNumber = teams.size % 2 == 0
    val fixture = mutableListOf(mutableListOf<Match>())

    fixture.clear()
    for (round in 0 until numberOfRound) {
        var home = ""
        var away = ""
        val roundTeams = mutableListOf<Match>()
        for (i in teams.indices) {
            if (i % 2 == 0) home = teams[i].name
            else {
                away = teams[i].name
                if (fixture.size < numberOfRound / 2) roundTeams.add(Match(home, away))
                else roundTeams.add(Match(away, home))
            }
        }

        fixture.add(roundTeams)

        val temp = teams[teams.size - 1]
        val downTo = if (isEvenNumber) 2 else 1
        for (i in teams.size - 1 downTo downTo) {
            teams[i] = teams[i - 1]
        }
        if (isEvenNumber) teams[1] = temp else teams[0] = temp
    }
    return fixture
}
