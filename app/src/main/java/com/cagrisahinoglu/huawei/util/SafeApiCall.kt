package com.cagrisahinoglu.huawei.util

import com.cagrisahinoglu.huawei.data.util.ApiResult
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext

suspend fun <T> safeApiCall(
    dispatcher: CoroutineDispatcher,
    api: suspend () -> T
): ApiResult<T> {
    return withContext(dispatcher) {
        ApiResult.Loading
        try {
            ApiResult.Success(api.invoke())
        } catch (e: Exception) {
            ApiResult.Error(e)
        }
    }
}
