package com.cagrisahinoglu.huawei.leaguefixture

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.cagrisahinoglu.huawei.domain.model.Match
import com.google.gson.Gson

class LeagueFixtureFragmentAdapter(
    private val fragment: Fragment,
    private val fixtureList: List<List<Match>>
) : FragmentStateAdapter(fragment) {

    private val gson = Gson()

    override fun getItemCount(): Int = fixtureList.size

    override fun createFragment(position: Int): Fragment {
        val fragment = LeagueFixturePagerFragment()
        val json = gson.toJson(fixtureList[position])
        fragment.arguments = Bundle().apply {
            putString("MATCH_OBJ", json)
        }
        return fragment
    }
}
