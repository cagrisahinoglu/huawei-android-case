package com.cagrisahinoglu.huawei.leaguefixture

import com.cagrisahinoglu.huawei.R
import com.cagrisahinoglu.huawei.base.BaseFragment
import com.cagrisahinoglu.huawei.databinding.FragmentLeagueFixturePagerBinding
import com.cagrisahinoglu.huawei.domain.model.Match
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class LeagueFixturePagerFragment : BaseFragment<FragmentLeagueFixturePagerBinding>() {

    private val gson = Gson()

    override fun getLayoutId(): Int = R.layout.fragment_league_fixture_pager

    override fun initViews() {
        arguments?.takeIf { it.containsKey("MATCH_OBJ") }?.apply {
            val itemType = object : TypeToken<List<Match>>() {}.type
            val fixtureWeek = gson.fromJson<List<Match>>(getString("MATCH_OBJ"), itemType)
            val matchAdapter = LeagueFixtureMatchAdapter()
            matchAdapter.submitList(fixtureWeek)
            binding.fragmentLeaguePagerRecyclerView.adapter = matchAdapter
        }
    }
}
