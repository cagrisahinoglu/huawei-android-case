package com.cagrisahinoglu.huawei.leaguefixture

import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import com.cagrisahinoglu.huawei.R
import com.cagrisahinoglu.huawei.base.BaseFragment
import com.cagrisahinoglu.huawei.databinding.FragmentLeagueFragmentBinding
import com.google.android.material.tabs.TabLayoutMediator

class LeagueFixtureFragment : BaseFragment<FragmentLeagueFragmentBinding>() {

    private val args: LeagueFixtureFragmentArgs by navArgs()
    private val viewModel: LeagueFixtureViewModel by viewModels()
    private lateinit var leagueFixtureFragmentAdapter: LeagueFixtureFragmentAdapter

    override fun getLayoutId(): Int = R.layout.fragment_league_fragment

    override fun initViews() {
        viewModel.createFixture(args.teamList.toList())
    }

    override fun observeEvents() {
        super.observeEvents()

        lifecycleScope.launchWhenResumed {
            viewModel.fixture.observe(viewLifecycleOwner) { fixture ->
                leagueFixtureFragmentAdapter = LeagueFixtureFragmentAdapter(
                    fragment = this@LeagueFixtureFragment,
                    fixtureList = fixture
                )
                with(binding) {
                    fragmentLeagueViewPager.adapter = leagueFixtureFragmentAdapter

                    TabLayoutMediator(
                        fragmentLeagueTabLayout,
                        fragmentLeagueViewPager
                    ) { tab, position ->
                        tab.text = getString(R.string.league_fixture_fragment_week, position + 1)
                    }.attach()
                }
            }
        }
    }
}
