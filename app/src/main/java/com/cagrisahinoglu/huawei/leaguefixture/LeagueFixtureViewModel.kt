package com.cagrisahinoglu.huawei.leaguefixture

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.cagrisahinoglu.huawei.domain.model.Match
import com.cagrisahinoglu.huawei.domain.model.Team
import com.cagrisahinoglu.huawei.util.createLeagueFixture
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class LeagueFixtureViewModel : ViewModel() {

    val fixture = MutableLiveData<List<List<Match>>>()

    fun createFixture(teamList: List<Team>) {
        viewModelScope.launch(Dispatchers.Default) {
            val data = createLeagueFixture(teamList)
            fixture.postValue(data)
        }
    }
}
