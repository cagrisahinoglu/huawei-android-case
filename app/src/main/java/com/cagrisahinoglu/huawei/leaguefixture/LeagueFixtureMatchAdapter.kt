package com.cagrisahinoglu.huawei.leaguefixture

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.cagrisahinoglu.huawei.R
import com.cagrisahinoglu.huawei.databinding.ItemLeagueFixtureBinding
import com.cagrisahinoglu.huawei.domain.model.Match

class LeagueFixtureMatchAdapter : ListAdapter<Match, FixtureMatchViewHolder>(Callback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FixtureMatchViewHolder {
        val view = DataBindingUtil.inflate<ItemLeagueFixtureBinding>(
            LayoutInflater.from(parent.context),
            R.layout.item_league_fixture,
            parent,
            false
        )
        return FixtureMatchViewHolder(view)
    }

    override fun onBindViewHolder(holder: FixtureMatchViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    object Callback : DiffUtil.ItemCallback<Match>() {
        override fun areItemsTheSame(oldItem: Match, newItem: Match): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: Match, newItem: Match): Boolean {
            return oldItem == newItem
        }

    }
}

class FixtureMatchViewHolder(
    private val binding: ItemLeagueFixtureBinding
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(match: Match) {
        binding.match = match
        binding.executePendingBindings()
    }
}
