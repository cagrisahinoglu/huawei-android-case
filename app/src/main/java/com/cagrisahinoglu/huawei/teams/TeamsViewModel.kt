package com.cagrisahinoglu.huawei.teams

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.cagrisahinoglu.huawei.data.util.ApiResult
import com.cagrisahinoglu.huawei.domain.model.Team
import com.cagrisahinoglu.huawei.domain.usecase.GetTeamUseCase
import com.cagrisahinoglu.huawei.util.safeApiCall
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class TeamsViewModel @Inject constructor(
    private val getTeamUseCase: GetTeamUseCase
) : ViewModel() {

    val teams = MutableLiveData<ApiResult<List<Team>>>()

    fun getTeams(teamSize: Int) {
        viewModelScope.launch {
            val result = safeApiCall(dispatcher = Dispatchers.IO) {
                getTeamUseCase.getTeams(teamSize)
            }
            teams.postValue(result)
        }
    }
}
