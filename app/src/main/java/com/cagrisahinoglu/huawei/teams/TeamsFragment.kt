package com.cagrisahinoglu.huawei.teams

import androidx.fragment.app.viewModels
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.cagrisahinoglu.huawei.R
import com.cagrisahinoglu.huawei.base.BaseFragment
import com.cagrisahinoglu.huawei.data.util.ApiResult
import com.cagrisahinoglu.huawei.databinding.FragmentTeamsBinding
import com.cagrisahinoglu.huawei.util.observeOnce
import com.cagrisahinoglu.huawei.util.setVisibility
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class TeamsFragment : BaseFragment<FragmentTeamsBinding>() {

    private val viewModel: TeamsViewModel by viewModels()
    private val args: TeamsFragmentArgs by navArgs()
    private lateinit var action: NavDirections
    private val teamsAdapter = TeamsAdapter()

    override fun getLayoutId(): Int = R.layout.fragment_teams

    override fun initViews() {
        viewModel.getTeams(args.teamSize)

        binding.teamsFragmentDrawButton.setOnClickListener {
            findNavController().navigate(action)
        }
    }

    override fun observeEvents() {
        super.observeEvents()

        viewModel.teams.observeOnce(this) { result ->
            when (result) {
                is ApiResult.Success -> {
                    with(binding) {
                        teamsFragmentProgressBar.setVisibility(false)
                        teamsAdapter.submitList(result.data)
                        teamsFragmentRecyclerView.adapter = teamsAdapter
                        teamsFragmentDrawButton.isEnabled = true
                    }
                    action = TeamsFragmentDirections.actionTeamsFragmentToLeagueFixtureFragment(
                        result.data.toTypedArray()
                    )
                }
                is ApiResult.Error -> {
                    with(binding) {
                        teamsFragmentProgressBar.setVisibility(false)
                        teamsFragmentDrawButton.isEnabled = false
                    }
                }
                is ApiResult.Loading -> {
                    with(binding) {
                        teamsFragmentProgressBar.setVisibility(true)
                        teamsFragmentDrawButton.isEnabled = false
                    }
                }
            }
        }
    }
}
