package com.cagrisahinoglu.huawei.teams

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.cagrisahinoglu.huawei.R
import com.cagrisahinoglu.huawei.databinding.ItemTeamBinding
import com.cagrisahinoglu.huawei.domain.model.Team

class TeamsAdapter : ListAdapter<Team, TeamsViewHolder>(Callback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TeamsViewHolder {
        val view = DataBindingUtil.inflate<ItemTeamBinding>(
            LayoutInflater.from(parent.context),
            R.layout.item_team,
            parent,
            false
        )
        return TeamsViewHolder(view)
    }

    override fun onBindViewHolder(holder: TeamsViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    object Callback : DiffUtil.ItemCallback<Team>() {
        override fun areItemsTheSame(oldItem: Team, newItem: Team): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: Team, newItem: Team): Boolean {
            return oldItem == newItem
        }

    }
}

class TeamsViewHolder(
    private val binding: ItemTeamBinding
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(team: Team) {
        with(binding) {
            teamModel = team
            executePendingBindings()
        }
    }
}
